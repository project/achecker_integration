<?php

/**
 * @file
 * Installation file for AChecker integration module.
 */

/**
 * Implements hook_install().
 */
function achecker_integration_install() {

  // All guidelines supported.
  $guides = array(
    'bitv-1.0-(level-2)'   => 'BITV1',
    'section-508'          => '508',
    'stanca-act;'          => 'STANCA',
    'wcag-1.0-(level-a)'   => 'WCAG1-A',
    'wcag-1.0-(level-aa)'  => 'WCAG1-AA',
    'wcag-1.0-(level-aaa)' => 'WCAG1-AAA',
    'wcag-2.0-l1'          => 'WCAG2-A',
    'wcag-2.0-l2'          => 'WCAG2-AA',
    'wcag-2.0-l3'          => 'WCAG2-AAA',
  );
  variable_set('achecker_integration_guides', $guides);

  // Default guideline used (WCAG1-AA).
  $default_guides = array();
  foreach ($guides as $guide => $name) {
    if ($guide == 'wcag-1.0-(level-aa)') {
      $default_guides[$guide] = 1;
    }
    else {
      $default_guides[$guide] = 0;
    }
  }
  variable_get('achecker_integration_scan_guides', $default_guides);
}

/**
 * Implements hook_requirements().
 */
function achecker_integration_requirements($phase) {

  $requirements = array();

  // Ensure translations don't break during installation.
  $t = get_t();

  switch ($phase) {

    case 'runtime':

      // Check for webservice ID.
      $requirements['achecker_integration_webservice_id'] = array('title' => $t('Achecker integration webservice id'));
      $webservice_id = variable_get('achecker_integration_webservice_id', '');
      if ($webservice_id) {
        $requirements['achecker_integration_webservice_id']['severity'] = REQUIREMENT_OK;
        $requirements['achecker_integration_webservice_id']['value']    = $t('Webservice id entered');
      }
      else {
        $requirements['achecker_integration_webservice_id']['severity']    = REQUIREMENT_ERROR;
        $requirements['achecker_integration_webservice_id']['value']       = $t('Webservice id not entered');
        $requirements['achecker_integration_webservice_id']['description'] = $t('Register for a <a href="!url_id">webservice id</a> and enter at the <a href="!url_settings">setting page</a>.', array(
          '!url_id'        => 'http://achecker.ca/register.php',
          '!url_settings'  => '/admin/config/content/achecker',
        ));
      }

    case 'install':

      // Ensure DomDocument is present to parse REST response.
      $requirements['achecker_integration_class_domdocument'] = array('title' => $t('Achecker integration DomDocument'));
      if (class_exists('DOMDocument')) {
        $requirements['achecker_integration_class_domdocument']['severity'] = REQUIREMENT_OK;
        $requirements['achecker_integration_class_domdocument']['value']    = $t('DomDocument present');
      }
      else {
        $requirements['achecker_integration_class_domdocument']['severity']    = REQUIREMENT_ERROR;
        $requirements['achecker_integration_class_domdocument']['value']       = $t('DomDocument not present');
        $requirements['achecker_integration_class_domdocument']['description'] = $t('Could not find class DomDocument to parse REST response. (see @url)', array(
          '@url' => 'http://www.php.net/manual/en/class.domdocument.php',
        ));
      }
      break;
  }
  return $requirements;
}

/**
 * Implements hook_schema().
 */
function achecker_integration_schema() {

  $schema['achecker_integration_scan'] = array(
    'description' => 'Stores all accessibilty scan responses. see http://achecker.ca/documentation/web_service_api.php#rest_sample_response_validation',
    'fields' => array(
      'nid'  => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'Node nid.',
      ),
      'vid'  => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'Node vid.',
      ),
      'http_code'  => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'Webservice response code.',
        'default'     => 0,
      ),
      'error_code'  => array(
        'type'        => 'int',
        'not null'    => FALSE,
        'description' => 'Validation error response.',
        'default'     => 0,
      ),
      'status' => array(
        'type'        => 'varchar',
        'not null'    => FALSE,
        'length'      => 16,
        'description' => 'Can be one of these values: FAIL, CONDITIONAL PASS, PASS.',
      ),
      'session' => array(
        'type'        => 'varchar',
        'not null'    => FALSE,
        'length'      => 64,
        'description' => 'The same ID must be sent back in make/reverse decisions request in response to the validation request. This is to ensure the make/reverse decision request comes from the authenticated source.',
      ),
      'error_qty'  => array(
        'type'        => 'int',
        'not null'    => FALSE,
        'description' => 'Counts the number of known problems.',
      ),
      'likely_qty'  => array(
        'type'        => 'int',
        'not null'    => FALSE,
        'description' => 'Counts the number of likely problems.',
      ),
      'potential_qty'  => array(
        'type'        => 'int',
        'not null'    => FALSE,
        'description' => 'Counts the number of potential problems.',
      ),
      'changed' => array(
        'type'        => 'int',
        'not null'    => FALSE,
        'default'     => 0,
        'description' => 'Timestamp of scanned vid.',
      ),
      'scanned' => array(
        'type'        => 'int',
        'not null'    => FALSE,
        'default'     => 0,
        'description' => 'Timestamp of scan.',
      ),

    ),
    'primary key' => array('vid'),
  );

  $schema['achecker_integration_scan_result'] = array(
    'description' => 'Stores all results from an accessibilty scan response.',
    'fields' => array(
      'rid' => array(
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
        'description' => 'Result id.',
      ),
      'nid'  => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'Node nid.',
      ),
      'vid' => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'Node vid.',
      ),
      'type' => array(
        'type'        => 'varchar',
        'not null'    => TRUE,
        'length'      => 32,
        'description' => 'Can be one of these values: Error, Likely Problem, Potential Problem.',
      ),
      'line' => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'Within the source code of the validated document, refers to the line where the error was detected.',
      ),
      'col' => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'Within the source code of the validated document, refers to the column of the line where the error was detected.',
      ),
      'message' => array(
        'type'        => 'text',
        'not null'    => TRUE,
        'description' => 'The actual error message.',
      ),
      'suggestion_id' => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'The suggestion number from AChecker (enables link to documentation).',
      ),
      'source' => array(
        'type'        => 'text',
        'not null'    => TRUE,
        'description' => 'The line of the source where the error/problem was detected.',
      ),
      'repair' => array(
        'type'        => 'text',
        'not null'    => FALSE,
        'description' => 'The actual message of how to repair. Only presented when type is "Error".',
      ),
      'sequence_id' => array(
        'type'        => 'varchar',
        'not null'    => FALSE,
        'length'      => 32,
        'description' => 'The unique sequence ID identifying each error/problem. This ID is used to pinpoint each error/problem in make/reverse decision request.',
      ),
      'decision_pass' => array(
        'type'        => 'text',
        'not null'    => FALSE,
        'description' => 'The actual text message of the pass decision. Only presented when resultType is "Likely Problem" or "Potential Problem".',
      ),
      'decision_fail' => array(
        'type'        => 'text',
        'not null'    => FALSE,
        'description' => 'The actual text message of the fail decision. Only presented when resultType is "Likely Problem" or "Potential Problem".',
      ),
      'decision_made' => array(
        'type'        => 'varchar',
        'not null'    => FALSE,
        'length'      => 8,
        'description' => 'Only presented when the decision has been made by user. Can be one of these two values: PASS, FAIL. PASS is set when pass decision is chosen by the user. Otherwise, FAIL is set.',
      ),
      'decision_made_date' => array(
        'type'        => 'varchar',
        'not null'    => FALSE,
        'length'      => 32,
        'description' => 'Only presented when the decision has been made by user. The date and time when the decision was made.',
      ),
    ),
    'primary key' => array('rid'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function achecker_integration_uninstall() {
  variable_del('achecker_integration_guidelines');
  variable_del('achecker_integration_scan_guides');
  variable_del('achecker_integration_webservice_id');
  variable_del('achecker_integration_webservice_url');
}
